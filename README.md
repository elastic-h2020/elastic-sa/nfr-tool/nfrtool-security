# NFRTool-Security

Besides from open source SIEM solution Wazuh, the whole security framework for ELASTIC comprises of other components built with the purpose of communicating/alerting the whole system about security incidents.

This project contains the main software components NFRTool-Security is comprised of:
* **Security alert Monitor**
* **Resource Manager**
* **Nuvla Security Connector**


## Security Alert monitor

Filtered security alerts are retrieved from Wazuh Manager's alerts file and immediatly publishe in a MQTT topic

## Resource Manager

COMMS Aplications that need to be securized are read from dataClay. With that information, plus the alerts gathered
from subscribing to de previously mentioned MQTT topic, The RM indicates which workers shoiuld be switched off from the ELASTIC System. 

## Nuvla Security Connector


## Requirements

* docker (Tested with version 19.03.6)
* docker-compose (Tested with version 1.26.0)
* non-valid for arm64 architectures


### Launch steps 

```console
# docker-compose up
```

## Installation | Launch steps (Centralized Security Wazuh Manager)

https://documentation.wazuh.com/current/docker/wazuh-container.html#production-deployment

### Launch steps

```console
# docker-compose up
```

## Installation steps | Launch steps (Wazuh Agent)

### Adding the Wazuh Repository

```console
# curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | apt-key add -
# echo "deb https://packages.wazuh.com/4.x/apt/ stable main" | tee -a /etc/apt/sources.list.d/wazuh.list
# apt-get update
```

### Install Wazuh agent

```console
# apt install wazuh-agent
```

### Enabling the service

```console
# systemctl daemon-reload
# systemctl enable wazuh-agent
# systemctl start wazuh-agent
```

# Validation

https://www.youtube.com/watch?v=Cx2rIpGA3V8



# Acknowledgements
This work has been supported by the EU H2020 project ELASTIC, contract #825473.