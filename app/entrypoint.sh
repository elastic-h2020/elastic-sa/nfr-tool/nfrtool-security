#!/bin/sh
set -e
# set -x

CONTRACT_ID_FILE=${DC_SHARED_VOLUME}/${NAMESPACE}_contractid
export DATACLAY_JAR="${DC_VOL_PATH}/dataclay.jar"
export CLASSPATH="${CLASSPATH}:${DATACLAY_JAR}"
export PATH="${PATH}:${DC_VOL_PATH}/entrypoints"

########################### create cfgfiles ###########################

mkdir -p "${DC_CONFIG_PATH}"
printf "HOST=%s\nTCPPORT=%d" "${LOGICMODULE_HOST}" "${LOGICMODULE_PORT_TCP}" > "${DC_CONFIG_PATH}/client.properties"
echo "Account=${USER}
Password=${PASS}
DataSets=${DATASET}
DataSetForStore=${DATASET}
StubsClasspath=${STUBSPATH}" > "${DC_CONFIG_PATH}/session.properties"

######################################################
# Wait for dataclay to be alive (max retries 10 and 5 seconds per retry)
dataclaycmd WaitForDataClayToBeAlive 10 5

# Wait for contract id in shared volume
while [ ! -f "${CONTRACT_ID_FILE}" ]; do echo "Waiting for contract ID at ${CONTRACT_ID_FILE}..."; sleep 5; done

# Get stubs
mkdir -p "${STUBSPATH}"
dataclaycmd GetStubs "${USER}" "${PASS}" "${NAMESPACE}" "${STUBSPATH}"

# Encapsulate stubs into JAR, then install
cd "${STUBSPATH}"
jar cf stubs.jar es/bsc/compss/nfr/model
mvn install:install-file -Dfile=stubs.jar -DgroupId=es.bsc.compss -DartifactId=nfrtool-dataclay-stubs -Dversion=2.4 -Dpackaging=jar -DcreateChecksum=true
cd -

mvn -f /root/nfrtool-security/ clean package -DskipTests
cp /root/nfrtool-security/target/nfrtool.jar /root/

# Execute command
exec "$@"
