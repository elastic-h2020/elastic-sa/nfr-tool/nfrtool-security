package rest;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;


public class RestCommunications {

    final URL loginURL = new URL(Optional.ofNullable(System.getenv("WAZUH_MANAGER_URL")).orElse("https://localhost:55000") + "/security/user/authenticate?raw=true");
    final URL agentListURl = new URL(Optional.ofNullable(System.getenv("WAZUH_MANAGER_URL")).orElse("https://localhost:55000")  + "/agents?select=ip,status");
    final URL scaURl = new URL(Optional.ofNullable(System.getenv("WAZUH_MANAGER_URL")).orElse("https://localhost:55000")  + "/sca/");
    final String user = Optional.ofNullable(System.getenv("WAZUH_USER")).orElse("wazuh");
    final String password = Optional.ofNullable(System.getenv("WAZUH_PASS")).orElse("wazuh");
    final String basicAuth = "Basic " + new String(Base64.getEncoder().encode(String.format("%s:%s", user, password).getBytes(StandardCharsets.UTF_8)));

    ArrayList<WazuhAgent> agentList = new ArrayList<>();

    public RestCommunications() throws MalformedURLException {}

    private String getResponse(URL url, Map<String, String> headers) throws IOException, KeyManagementException, NoSuchAlgorithmException {

        URLConnection con;
        BufferedReader in;
        String inputLine;
        StringBuffer content;

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                }
        };

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

        con = url.openConnection();
        headers.forEach((k, v) -> con.setRequestProperty(k,v));
        try {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            return content.toString();
        }
        catch(IOException e) {
            System.out.println("[-] Error REST API -> " + e.getMessage());
            System.exit(1);
        }
        catch (Exception e) {
            System.out.println("[-] Error REST API -> " + e.getMessage());
            System.exit(1);
        }

        return null;
    }

    public ArrayList<WazuhAgent> getAgents() throws NoSuchAlgorithmException, IOException, KeyManagementException {

        Map<String,String> headers = new HashMap<>();

        headers.putIfAbsent("Authorization", basicAuth);
        String response = getResponse(loginURL, headers);
        if(response != null) {
            headers.put("Authorization", "Bearer " + response);
            String jsonResponse = getResponse(agentListURl, headers);
            JSONObject json = new JSONObject(jsonResponse);
            JSONArray array = json.getJSONObject("data").getJSONArray("affected_items");
            for (int i = 0; i < array.length(); i++) {
                WazuhAgent agent = new WazuhAgent(
                        array.getJSONObject(i).getString("id"),
                        array.getJSONObject(i).getString("ip"),
                        array.getJSONObject(i).getString("status"));
                agentList.add(agent);
            }

            return agentList;
        }

        else
            return null;
    }

    public void getSCAResults() throws IOException, NoSuchAlgorithmException, KeyManagementException {

        Map<String,String> headers = new HashMap<>();

        headers.putIfAbsent("Authorization", basicAuth);
        headers.put("Authorization", "Bearer " + getResponse(loginURL, headers));

        for(WazuhAgent agent: agentList) {
            String jsonResponse = getResponse(new URL(scaURl, agent.getId()), headers);
            JSONObject json = new JSONObject(jsonResponse);
            JSONArray scaArray = json.getJSONObject("data").getJSONArray("affected_items");
            for (int j = 0; j < scaArray.length(); j++) {
                agent.getScaResult().add(new SCAResult(
                        scaArray.getJSONObject(j).getString("name"),
                        scaArray.getJSONObject(j).getInt("score")));
            }
           // System.out.println(agent.toString());
        }
    }

    public ArrayList<WazuhAgent> getAgentList() {
        return agentList;
    }

    public void setAgentList(ArrayList<WazuhAgent> agentList) {
        this.agentList = agentList;
    }
}
