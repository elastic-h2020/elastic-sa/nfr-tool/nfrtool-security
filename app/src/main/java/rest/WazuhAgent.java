package rest;

import java.util.ArrayList;

public class WazuhAgent {
    String id;
    String ip;
    String status;
    ArrayList<SCAResult> scaResult;

    public WazuhAgent(String id, String ip, String status) {
        this.id = id;
        this.ip = ip;
        this.status = status;
        scaResult = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SCAResult> getScaResult() {
        return scaResult;
    }

    public void setScaResult(ArrayList<SCAResult> scaResult) {
        this.scaResult = scaResult;
    }

    @Override
    public String toString() {
        return "WazuhAgent{" +
                "id='" + id + '\'' +
                ", ip='" + ip + '\'' +
                ", status='" + status + '\'' +
                ", scaResult=" + scaResult +
                '}';
    }
}
