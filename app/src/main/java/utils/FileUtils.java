package utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class FileUtils {

	File folder;

	public FileUtils(String folderPath) {

		this.folder = new File(folderPath);
	}

	public NodeList readXML(String expression, File file) { // example: //score

		NodeList nodeList = null;

		try {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;

			dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();

			XPath xPath = XPathFactory.newInstance().newXPath();

			nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);

		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return nodeList;
	}
	
	/*
	 * Get score from XML file
	 */

	public float getScore(NodeList nodeList) {

		Node node = nodeList.item(0); // There is just one score field in the whole XML file

		return Float.parseFloat(node.getTextContent());
	}

	public ArrayList<Float> getScores() {

		ArrayList<Float> scores = new ArrayList<Float>(); 

		for(String subfolder : listFolder()) {
			for(String file : listFile(subfolder)) {
				scores.add(getScore(readXML("\\score", new File(file))));
			}
		}

		return scores;
	}
	
	/*
	 * Directory Creation for scanned nodes
	 */

	public Path createDirectory(es.bsc.compss.nfr.model.Node node) {

		Path path = Paths.get(this.folder + "\\" + node.getID());

		if (!Files.exists(path)) {
			try {
				return Files.createDirectories(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return path;
	}

	public void createNodeDirectories(ArrayList<es.bsc.compss.nfr.model.Node> nodes, String folder) {
		nodes.forEach(node -> createDirectory(node));
	}

	/*
	 * Folder iteration methods
	 */
	
	public List<String> listFolder() {

		try (Stream<Path> walk = Files.walk(Paths.get(this.folder.toURI()))) {

			List<String> result = walk.filter(Files::isDirectory)
					.map(x -> x.toString()).collect(Collectors.toList());

			result.forEach(System.out::println);
			result.remove(0); // Remove first item, it is the root folder itself!

			return result;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<String> listFile(String subfolder) {

		try (Stream<Path> walk = Files.walk(Paths.get(subfolder))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".xml")).collect(Collectors.toList());

			result.forEach(file -> System.out.println("\t" + result));

			return result;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	

	
}
