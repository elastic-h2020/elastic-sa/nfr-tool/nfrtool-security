package utils;

import java.io.IOException;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

public class
SshUtils {

	public void connect(String hostname) {
		
	
	   SSHClient ssh = new SSHClient();
	   ssh.addHostKeyVerifier(new PromiscuousVerifier());
      
       try {
    	   ssh.loadKnownHosts();
    	   ssh.connect(hostname);
	       try {
	           ssh.authPublickey(System.getProperty("user.name"));
	       } finally {
	           ssh.disconnect();
	       }
		} catch (IOException e) {
			e.printStackTrace();
		}
  
	}    
}
