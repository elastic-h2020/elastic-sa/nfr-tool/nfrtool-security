package resourceManager;

import java.util.ArrayList;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;

public abstract class ResourceManager {
	
	ElasticSystem elasticSystem;
	ArrayList<COMPSsApplication> apps;
	ArrayList<Node> nodes;
	
	/*
	 * Public Getters & Setters
	 */
	
	public ResourceManager(ElasticSystem elasticSystem) {
		
		this.elasticSystem = elasticSystem;
		this.apps = this.elasticSystem.getApplications();
		this.nodes = elasticSystem.getNodes();
	}

	public ElasticSystem getElasticSystem() {
		return elasticSystem;
	}

	public void setElasticSystem(ElasticSystem elasticSystem) {
		this.elasticSystem = elasticSystem;
	}

	public ArrayList<COMPSsApplication> getApps() {
		return apps;
	}

	public void setApps(ArrayList<COMPSsApplication> apps) {
		this.apps = apps;
	}

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}


	public static <T> Collector<T, ?, T> toSingleton() {
		return Collectors.collectingAndThen(
				Collectors.toList(),
				list -> {
					if (list.size() == 0) {
						throw new IllegalStateException();
					}
					return list.get(0);
				}
		);
	}
}
