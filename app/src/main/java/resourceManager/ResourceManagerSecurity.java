package resourceManager;

import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import rest.RestCommunications;
import rest.WazuhAgent;

import java.io.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import org.json.*;


public class ResourceManagerSecurity extends ResourceManager implements MqttCallback {

	String root = System.getProperty("user.dir");
	String cfgPath = root + File.separator + "cfgfiles";
	String propsPath =  cfgPath + File.separator + "app.properties";
	private static final String GRM_TOPIC = "violations";
	String subsTopic;

	public MqttClient mqttClient;
	private MqttClient extMQClient;

	public ResourceManagerSecurity(ElasticSystem elasticSystem) throws MqttException {
		super(elasticSystem);
		Properties props = new Properties();

		try {
			props.load(new FileInputStream(propsPath));
		} catch (IOException e) {
			e.printStackTrace();
		}

		subsTopic = props.getProperty("SUBSCRIBE_TOPIC");
		this.initializeMqttClient();
	}

	// TODO For now it is a dummy function
	public boolean determineSecurityLevel(int score) {
		// If SCA is too low we should deactivate the node (SCA checks the SO, not the events!)
		// SCA is between 0-100
		if(score < 10)
			return false;
		return true;
	}

	// TODO For now, all workers for a specific application
	public void modifyWorkersStatus(Node node, boolean isSecure) {
		//node.getWorkers().stream().filter(Worker::isActive).forEach(worker -> worker.setActive(isSecure));

	}

	@Override
	public void connectionLost(Throwable throwable) {
		System.err.println("[-] The connection to the MQTT broker was lost. Attempting reconnection");
		int tries = 5;
		while (tries-- > 0 && !this.mqttClient.isConnected()) {
			try {
				this.mqttClient.connect();
				System.out.println("Connection recovered!");
			} catch (MqttException e) {
				System.err.println("The reconnection failed. (" + e.getMessage() + ") Tries left: " + tries);
				try { Thread.sleep(3000); } catch (Exception e2) { }
				continue;
			}
			break;
		}
		if (!this.mqttClient.isConnected()) {
			System.err.println("All reconnection attempts failed. Exiting NFRTool");
			System.exit(1);
		} else {
			System.out.println("Connection with the MQTT broker is established");
		}
	}

	@Override
	public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		final String message = new String(mqttMessage.getPayload());
		System.out.printf("\n[?] An ALERT  message has arrived on topic '%s %s'\n", s, message);

		int level = 0;
		String node_ip = "";
		try {
			JSONObject json = new JSONObject(message);
			level = json.getInt("level");
			node_ip = json.getString("ip");
		} catch (Exception e) {
			System.out.println("JSON bad formatted");
			return;
		}

		// Check if the event has a very high level (compromised node), level goes from 1-15
		if (level < 10) {
			System.out.println("Ignoring message... level too low");
			return;
		}

		RestCommunications rc = null;
		ArrayList<WazuhAgent> agentList = new ArrayList<>();

		try {
			final String ip = node_ip;
			Node node = this.getNodes().stream().filter(n -> n.getIpWifi().equals(ip)
					|| n.getIpLte().equals(ip) || n.getIpEth().equals(ip)).collect(ResourceManager.toSingleton());

			System.out.println(String.format("Violation message sent to GRM: {\"workerIP\": %s, \"workerID\": %s, \"dimension\": \"security\"}", node_ip, "*"));
			final String jsonMessageContent = String.format("{\"nodeIP\": \"%s\", \"dimension\": \"security\"}", node_ip);
			
			// Deactivate node in dataclay
			node.setIsSecure(false);
			System.out.println("[?] Node: " + node_ip + "\n\t-- Event level: " + String.valueOf(level) +
							"\n\t[?] isSecure Node flag: " + node.isSecure() + "\n\n**************************\n");

			// Send event message to GRM
			final MqttMessage violationMsg = new MqttMessage(jsonMessageContent.getBytes());
			extMQClient.publish(GRM_TOPIC, violationMsg);
		}
		catch (IllegalStateException e) {
			System.out.println("[-] MQTT Message IP does NOT match with any ELASTIC nodes IPs");;
		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {}

	private void initializeMqttClient() throws MqttException {

		final String broker = Optional.ofNullable(System.getenv("MQTT_BROKER_URL")).orElse("tcp://localhost:1883");
		final String extBroker = Optional.ofNullable(System.getenv("CENTRAL_MQ_URL")).orElse("tcp://localhost:1883");
		final String clientId = UUID.randomUUID().toString();
		final String extClientId = UUID.randomUUID().toString();

		final MqttConnectOptions connectOptions = new MqttConnectOptions();
		connectOptions.setCleanSession(true);  // Totally optional

		this.mqttClient = new MqttClient(broker, clientId, new MemoryPersistence());
		this.extMQClient = new MqttClient(extBroker, extClientId, new MemoryPersistence());
		this.mqttClient.setCallback(this);
		this.mqttClient.connect(connectOptions);
		this.mqttClient.subscribe(subsTopic);

		this.extMQClient.connect(connectOptions);
	}
}
