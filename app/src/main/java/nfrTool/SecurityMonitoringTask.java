package nfrTool;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import es.bsc.compss.nfr.model.Node;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;
import resourceManager.ResourceManager;
import resourceManager.ResourceManagerSecurity;
import rest.RestCommunications;
import rest.WazuhAgent;
import utils.FileUtils;

public class SecurityMonitoringTask extends TimerTask {

	ResourceManagerSecurity resourceManager;
	FileUtils fileUtils;

	public SecurityMonitoringTask(ResourceManagerSecurity resourceManager) {
		
		this.resourceManager = resourceManager;
	}

   @Override
   public void run() {

	   RestCommunications rc = null;
	   ArrayList<WazuhAgent> agentList = new ArrayList<>();

	   try {
		   rc = new RestCommunications();
		   agentList.addAll(rc.getAgents());
		   rc.getSCAResults();
	   } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
		   System.out.println("[-] Error in SecurityMonitoringTask -> " + e.getMessage());
	   } catch (NullPointerException npe) {
	   		System.out.println("[-] Agent list is empty");
	   }
	   try {
			for(WazuhAgent agent : agentList) {
				// TODO: Evaluate multiple scores?
				if(agent.getScaResult().size() != 0) {
					int score = agent.getScaResult().get(0).getScore();

					try {
						Node node = this.resourceManager.getNodes().stream().filter(n -> n.getIpWifi().equals(agent.getIp())
						|| n.getIpLte().equals(agent.getIp()) || n.getIpEth().equals(agent.getIp())).collect(ResourceManager.toSingleton());
						boolean isNodeSecure = this.resourceManager.determineSecurityLevel(score);
						this.resourceManager.modifyWorkersStatus(node, isNodeSecure);
						
						// If node is NOT secure, it has to remain NOT SECURE
						if (node.isSecure()) {
							node.setIsSecure(isNodeSecure);	
						}

						System.out.println("[?] Node: " + agent.getIp() + "\n\t-- Security Score (%): " + String.valueOf(score) +
							"\n\t[?] isSecure Node flag: " + node.isSecure() + "\n\n**************************\n");
					}
					catch (IllegalStateException e) {
						System.out.println("[-] Wazuh agents IP does NOT match with any ELASTIC nodes IPs");;
					}
				}
			}

	   } catch (ObjectNotRegisteredException onr) {
		   System.out.println("[-] Error (ObjectNotRegisteredException): " + onr.getMessage());
	   } catch (Exception e) {
		   System.out.println("[-] Generic Error: " + e.getMessage());
	   }
	}
}
