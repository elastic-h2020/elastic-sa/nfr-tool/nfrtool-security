package nfrTool;

import java.io.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import es.bsc.compss.nfr.model.COMPSsApplication;
import es.bsc.compss.nfr.model.ElasticSystem;
import es.bsc.compss.nfr.model.Node;
import es.bsc.compss.nfr.model.Worker;
import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.api.DataClayException;
import es.bsc.dataclay.exceptions.metadataservice.ObjectNotRegisteredException;
import org.eclipse.paho.client.mqttv3.MqttException;
import resourceManager.ResourceManagerSecurity;


public class NFRTool {

	private static final String SYSTEM_ALIAS = "system";

	private static ElasticSystem initializeMockData() {

		// App A definition:
		String name = "AppA";
		String uuid = "A";
		final int monitoringPeriod = 1;
		String infoNature = "delaySensitive";

		COMPSsApplication appA = new COMPSsApplication(name, uuid, monitoringPeriod);
		appA.setInfoNature(infoNature);

		// App B definition:
		name = "AppB";
		uuid = "B";
		infoNature = "TCPservices";

		COMPSsApplication appB = new COMPSsApplication(name, uuid, monitoringPeriod);
		appB.setInfoNature(infoNature);


		String ipWifi = "127.0.0.1";
		String ipLte = "172.10.0.2";
		//String ipEth = "192.168.137.17"; // TODO: check value
		String ipEth = "10.0.2.15"; // TODO: check value
		float cpuThreshold = 0;
		float energyThreshold = 0;
		float signalWifi = -30;
		int numCores = 4;

		Node node1 = new Node(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);

		// Node 2 definition:
		ipWifi = "0.0.0.0";
		ipLte = "172.10.0.4";
		ipEth = "172.16.124.69"; // TODO: check value
		cpuThreshold = 0;
		energyThreshold = 0;
		signalWifi = -30;
		numCores = 4;

		Node node2 = new Node(ipWifi, ipEth, ipLte, cpuThreshold, energyThreshold, signalWifi, numCores);


		// Worker 1A definition:
		final int pid1A = 32145;
		final boolean active1A = true;
		final float cpuUsage1A = 0;
		final float energyUsage1A = 0;
		final int computingUnits1A = 4;
		final float communicationCost1A = 0;

        Worker worker1A = new Worker(node1, pid1A, active1A, appA,
				cpuUsage1A, energyUsage1A, computingUnits1A, node1.getIpEth(), communicationCost1A, new ArrayList<>());

		appA.addWorker(worker1A);

		// Worker 1B definition:
		final int pid1B = 32146;
		final boolean active1B = true;
		final float cpuUsage1B = 0;
		final float energyUsage1B = 0;
		final int computingUnits1B = 4;
		final float communicationCost1B = 0;

		Worker worker1B = new Worker(node1, pid1B, active1B, appB, cpuUsage1B, energyUsage1B, computingUnits1B, node1.getIpWifi(),
				communicationCost1B, new ArrayList<>());
		// Attach worker to Node and COMPSsApplication
		appB.addWorker(worker1B);

		// Worker 2A definition:
		final int pid2A = 32147;
		final boolean active2A = true;
		final float cpuUsage2A = 0;
		final float energyUsage2A = 0;
		final int computingUnits2A = 4;
		final float communicationCost2A = 0;

		Worker worker2A = new Worker(node2, pid2A, active2A, appA, cpuUsage2A, energyUsage2A, computingUnits2A, node2.getIpEth(),
				communicationCost2A, new ArrayList<>());
		appA.addWorker(worker2A);

		// Worker 2B definition:
		final int pid2B = 32148;
		final boolean active2B = true;
		final float cpuUsage2B = 0;
		final float energyUsage2B = 0;
		final int computingUnits2B = 4;
		final float communicationCost2B = 0;

		Worker worker2B = new Worker(node2, pid2B, active2B, appB, cpuUsage2B, energyUsage2B, computingUnits2B, node2.getIpEth(),
				communicationCost2B, new ArrayList<>());
		appB.addWorker(worker2B);

		ArrayList<Node> systemNodes = new ArrayList<>(Arrays.asList(node1, node2));
		ArrayList<COMPSsApplication> systemApps = new ArrayList<>(Arrays.asList(appA, appB));

		return new ElasticSystem(systemApps, systemNodes);
	}


	public static void main(String[] args) throws DataClayException, ObjectNotRegisteredException, IOException, NoSuchAlgorithmException, KeyManagementException {

		System.out.println("[?] App started...");
		String root = System.getProperty("user.dir");
		String cfgPath = root + File.separator + "cfgfiles";
		String propsPath =  cfgPath + File.separator + "app.properties";

		Properties props = new Properties();
		
		try {
			props.load(new FileInputStream(propsPath));
			long period = Long.parseLong(props.getProperty("period"));

			System.out.println("[?] Starting Dataclay Client...");
			DataClay.init();
			System.out.println("[?] Dataclay Client started");

			ElasticSystem system;

			try {
				system = ElasticSystem.getByAlias(SYSTEM_ALIAS);
				System.out.println("ElasticSystem detected");
				System.out.println("\tNodes: " + system.getNodes().stream()
						.map(n -> String.format("{%s,%s}", n.getIpEth(), n.getIpWifi()))
						.collect(Collectors.joining(", ")));

			} catch (ObjectNotRegisteredException e) {
				System.out.println("No ElasticSystem detected");
				system = initializeMockData();
				System.out.println("New ElasticSystem with mock data was created.");
			}

			NFRMonitor nfrMonitor = new NFRMonitor(new ResourceManagerSecurity(system), period);
			nfrMonitor.runMonitor();

		} catch (DataClayException dce) {
			System.out.println(dce.getMessage());
		} catch (ObjectNotRegisteredException onr) {
			System.out.println(onr.getMessage());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
