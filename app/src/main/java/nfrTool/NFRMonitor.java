package nfrTool;

import java.util.Timer;

import es.bsc.dataclay.api.DataClay;
import es.bsc.dataclay.api.DataClayException;
import resourceManager.ResourceManagerSecurity;

public class NFRMonitor {
	
	ResourceManagerSecurity resourceManager;
	long period;

	public NFRMonitor(ResourceManagerSecurity resourceManager, long period) {
		
		this.resourceManager = resourceManager;
		this.period = period;
	}
	
	public void runMonitor() {
		
		final Timer timer = new Timer();

		SecurityMonitoringTask mTask = new SecurityMonitoringTask(this.resourceManager);
		timer.scheduleAtFixedRate(mTask, 0, this.period);
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
	        try {
	            timer.cancel();
	            DataClay.finish();
	        } catch (DataClayException e) {
	            e.printStackTrace();
	        }
		}));
	}
}
